#React challenge

Esse é um desafio de React que serve para um propósito:
*Nos mostrar suas habilidades*

---

## O desafio

O desafio é criar uma um fluxo simples de listagem de usuário e, quando selecionado, mostrar as informações em detalhes.

O que você precisa fazer:

* Crie um layout baseado nas imagens abaixo
* Estilize a página de forma consistente e bonita, seguindo o mais próximo possível do layout
* Consuma a API (https://private-3c5d2-fronttest.apiary-mock.com/users) para recuperar a lista de usuários

###Lista de usuários:
O layout da tela deve seguir o modelo abaixo mostrando quantas linhas forem necessárias.
![Lista de usuários](images/listaUsuarios.png) 

###Detalhe do usuário:
Ao clicar em um usuário da lista deve ser exibido a tela com mais informações deste usuário
![Detalhe do usuário](images/detalheUsuario.png)

---

## Informações adicionais

1. Faça um fork desse repositório
2. Quando finalizar nos envie o link para o seu repositório
3. Deixe comentado os lugares em que você teve dúvida de como proceder

---

## Bonus

1. Testes
2. Clean code
3. Uso de boas práticas
4. Utilizar Redux para guardar e exibir as informações