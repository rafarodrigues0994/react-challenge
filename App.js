import React, { Component } from 'react';
import DetalheUsuario from './DetalheUsuario';
import logo from './logo.svg';
import './App.css';

const API_KEY = "";

/*Confesso que nunca tive contato com a tecnologia e
infelizmente não tive tempo para conseguir concluir o desafio
de qualquer forma estou enviando o que fiz até então
Dificuldade: Extrair os dados do JSON da API
*/



class App extends Component {


  constructor(props){
    super(props);
    this.state = {
      isLoading: true,
      contacts: []
    }
  }

  ComponentDidMount(){
    this.fechData();
  }

  fetchData(){
    fetch('https://private-3c5d2-fronttest.apiary-mock.com/users')
    .then(response => response.json())
    .then(parsedJSON => parsedJSON.result.map(user =>(
    {
      name: `${user.name}`,
      photo: `${user.photo}`,
      email: `${user.email}`,
      address: `${user.address}`,
      phone: `${user.phone}`
    }

      )))
    .then(contacts => this.setState({
      contacts,
      isLoading: false
    }))
    .cath(error => console.log('parsing failed', error))
  
  }



  render() {
  
    return (
      <div className="App-contato">
        
      <div class="circle">
        <a href = "./DetalheUsuario"><img src="https://randomuser.me/api/portraits/med/men/19.jpg"/></a>
      </div>
        
        <h1 className="App-name">Simon Nielson</h1>
        <h1 className="App-conteudo">simon.nielsen@example.com</h1>

        
      </div>
    );
  }
}

export default App;
